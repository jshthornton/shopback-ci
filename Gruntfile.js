module.exports = function(grunt) {
	grunt.initConfig({
		'sftp-deploy': {
			prod: {
				auth: {
					host: 'ec2-52-74-88-2.ap-southeast-1.compute.amazonaws.com',
					port: 22,
					authKey: 'key'
				},
				cache: false,
				src: __dirname,
				exclusions: [
					'.git',
					'vendor',
					'node_modules',
					'composer.lock',
					'key.pem'
				],
				dest: 'shopback',
				progress: true
			}
		},

		sshexec: {
			phpunit: {
				command: 'cd shopback && vendor/bin/phpunit',
				options: {
					host: 'ec2-52-74-88-2.ap-southeast-1.compute.amazonaws.com',
					username: 'ubuntu',
					privateKey: grunt.file.read('key.pem')
				}
			},
			e2e: {
				command: 'cd shopback && nodejs node_modules/.bin/jasmine-node tests/e2eSpec.js',
				options: {
					host: 'ec2-52-74-88-2.ap-southeast-1.compute.amazonaws.com',
					username: 'ubuntu',
					privateKey: grunt.file.read('key.pem')
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-sftp-deploy');
	grunt.loadNpmTasks('grunt-ssh');
};
