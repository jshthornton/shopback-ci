var webdriver = require('selenium-webdriver'),
    By = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until;

describe('test', function() {
	function createDriver() {
		return new webdriver.Builder()
		    .forBrowser('firefox')
		    .build();
	};
	var driver;

	beforeEach(function() {
		driver = createDriver();
		jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;
	});
	it('Should have title', function(done) {
		driver.get('https://www.google.com.sg/').then(function() {
			var searchBox = driver.findElement(webdriver.By.name('q'));
			searchBox.sendKeys('webdriver');
			searchBox.getAttribute('value').then(function(value) {
				expect(value).toEqual('webdriver');
				done();
			});
		});
	});

	afterEach(function() {
		driver.quit();
	});
});
